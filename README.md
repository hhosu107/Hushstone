# Hushstone

So yeah.. uh.. This is and interface between Hearthstone and the Hush buttplug.

## Requirements
* Windows 10 with CREATORS UPDATE (needed for the new BLE funcionality).
* Bluetooth Low-Energy compatible adapter. Every Bluetooth 4.0 should have it, but I'm not 100% certain.
* Edited Hearthstone's log.config -> SEE INSTALLATION
* You **MIGHT** need to make windows aware of the device by adding it in the bluetooth settings. You can't *pair* hush, but you can add it to your devices this way.

Hush is named "LVS-*something*"
## Installation
1) For this to work, you need to have the following lines in your log.config file:

```
[Power]
LogLevel=1
FilePrinting=False
ConsolePrinting=True
ScreenPrinting=False
[Zone]
LogLevel=1
FilePrinting=False
ConsolePrinting=True
ScreenPrinting=False
```

Path to log.config should be `%LocalAppdata%\Blizzard\Hearthstone\log.config`. If it isn't there, create it.

## Building
Unfortunately, you can't just share UWP apps without putting them to windows store (there *might be* a way. I'm still researching). That means you have to build it yourself.
It's not that hard:

1) Get Visual Studio (I use 2017 community edition)

2) Import the project, either using GIT or by downloading it as a ZIP file

3) Run it. Don't forget to turn your Hush on.

4) Post any issues here so I can fix them
