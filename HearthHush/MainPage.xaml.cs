﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.ApplicationModel.Core;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HearthHush {
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainPage : Page {
		public MainPage() {
			InitializeComponent();
			//Set desired dimensions of the window
			ApplicationView.PreferredLaunchViewSize = new Size(512, 450);
			ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;
			ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(512, 450));
			LookForHush();
			WatchBatteryLevel();
		}

		public static async Task CallOnUiThreadAsync(CoreDispatcher dispatcher, DispatchedHandler handler) => await dispatcher.RunAsync(CoreDispatcherPriority.Normal, handler);
		public static async Task CallOnMainViewUiThreadAsync(DispatchedHandler handler) => await CallOnUiThreadAsync(CoreApplication.MainView.CoreWindow.Dispatcher, handler);

		private async void ChangeBatteryLevel(ProgressBar b, TextBlock t, byte batteryLevel) {
			await CallOnMainViewUiThreadAsync(() => {
				b.Value = batteryLevel;
				t.Text = Math.Round((((double)batteryLevel) / 127 * 100),0) + "%";
			});
		}

		private async void ChangeVisibility(UIElement el, Visibility vis) {
			await CallOnMainViewUiThreadAsync(() => {
				el.Visibility = vis;
			});
		}

		/// <summary>
		/// Initializes (finds) a lovense device (Hush probably) and enables the "READ" button (actually disables a blocker that blocks the button) if Hush is initialized.
		/// </summary>
		public async void LookForHush() {
			var r = (Windows.UI.Xaml.Shapes.Rectangle)FindName("FindHushRect");
			var t = (TextBlock)FindName("FindHushText");
			while (true) {
				if (LogReader.Hush == null) {
					LogReader.FindHush();
				}
				while (LogReader.Hush == null) {
					if (r.Visibility == Visibility.Collapsed) {
						ChangeVisibility(r, Visibility.Visible);
					}
					if (t.Visibility == Visibility.Collapsed) {
						ChangeVisibility(t, Visibility.Visible);
					}
					await Task.Delay(500);
				}
				if (LogReader.VibeChar == null) {
					LogReader.FindServices();
				}
				while (LogReader.VibeChar == null) {
					if (r.Visibility == Visibility.Collapsed) {
						ChangeVisibility(r, Visibility.Visible);
					}
					if (t.Visibility == Visibility.Collapsed) {
						ChangeVisibility(t, Visibility.Visible);
					}
					await Task.Delay(500);
				}
				if (r.Visibility == Visibility.Visible) {
					ChangeVisibility(r, Visibility.Collapsed);
				}
				if (t.Visibility == Visibility.Visible) {
					ChangeVisibility(t, Visibility.Collapsed);
				}
				await Task.Yield();
			}
		}

		/// <summary>
		/// Asks Hush for battery level every 10 sec and displays it in the app.
		/// </summary>
		public async void WatchBatteryLevel() {
			var b = (ProgressBar)FindName("battery");
			var t = (TextBlock)FindName("batteryLevel");
			while (true) {
				while (LogReader.BatteryChar == null) {
					await Task.Delay(500);
				}
				ChangeBatteryLevel(b,t,DataReader.FromBuffer((await LogReader.BatteryChar.ReadValueAsync()).Value).ReadByte());
				await Task.Delay(10000);
			}

		}

		private void panic(object sender, RoutedEventArgs e) {
			LogReader.Panic();
		}

		private void unPanic(object sender, RoutedEventArgs e) {
			LogReader.panicMode = false;
		}

		private void appBarToggleButton_Checked(object sender, RoutedEventArgs e) {
			ToggleButton button = sender as ToggleButton;
			if ((bool)button.IsChecked) {
				LogReader.Read();
			} else {
				LogReader.Stop();
			}
		}

		private void appBarToggleButton_Unchecked(object sender, RoutedEventArgs e) {
			LogReader.Stop();
		}

		private void vibrateFriendToggle(object sender, RoutedEventArgs e) {
			LogReader.vibrateOnFriendly = toggleSwitch.IsOn;
		}

		private void vibrateEnemyToggle(object sender, RoutedEventArgs e) {
			LogReader.vibrateOnEnemy = toggleSwitch1.IsOn;
		}

		private void vibratePackToggle(object sender, RoutedEventArgs e) {
			LogReader.vibrateOnPack = toggleSwitch2.IsOn;
		}

		private void logToFileToggle(object sender, RoutedEventArgs e) {
			LogReader.logToFile = toggleSwitch3.IsOn;
		}

		private void fDamageTakerToggled(object sender, RoutedEventArgs e) {
			if (fDamageTakerSwitch.IsOn) {
				LogReader.friendlyTaker = LogReader.Target.ENEMY;
			} else {
				LogReader.friendlyTaker = LogReader.Target.FRIEND;
			}
		}

		private void eDamageTakerToggled(object sender, RoutedEventArgs e) {
			if (eDamageTakerSwitch.IsOn) {
				LogReader.enemyTaker = LogReader.Target.ENEMY;
			} else {
				LogReader.enemyTaker = LogReader.Target.FRIEND;
			}
		}

		private void delayChanged(object sender, RangeBaseValueChangedEventArgs e) {
			LogReader.delay = (int)delay.Value;
		}
	}
}
