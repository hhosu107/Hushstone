﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Windows.Storage;
using Windows.Storage.Search;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Devices.Bluetooth;
using Windows.Storage.Streams;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using System.IO;
using System.Text.RegularExpressions;
namespace HearthHush {
	class LogReader {
		//Internal types
		public enum Target {
			FRIEND = 1,
			ENEMY = 2
		}

		public enum DataType {
			DAMAGE, CARD_REVEAL
		}

		public class CancelTask {
			public bool stop = false;
		}

		public struct Player {
			public byte playerID;
			public string playerName;

			public override string ToString() {
				return playerID + " - " + playerName;
			}
		}

		public struct VibrationData {
			public Target target;
			public DataType type;
			public int strenght;

			public VibrationData(Target target, DataType type, int strenght) {
				this.target = target;
				this.type = type;
				this.strenght = strenght;
			}

			public override string ToString() {
				return type + " " + target + " - " + strenght;
			}
		}

		//Reading?
		private static bool reading = false;
		public static bool Reading {
			get {
				return reading;
			}
		}

		//Privates
		private static Windows.Storage.Streams.IRandomAccessStream file;
		private static ulong lastIndex;
		private static StorageFile ClosedFile;
		private static CancelTask stopVibeTask;

		//Settings
		public static bool panicMode;
		public static bool vibrateOnFriendly;
		public static Target friendlyTaker = Target.FRIEND;
		public static bool vibrateOnEnemy;
		public static Target enemyTaker = Target.FRIEND;
		public static bool vibrateOnPack;
		public static bool logToFile;
		public static int delay = 400;

		public static GattCharacteristic VibeChar;
		public static GattCharacteristic BatteryChar;
		public static BluetoothLEDevice Hush;

		public static Player player = new Player();
		public static Player opponent = new Player();

		public static Regex findPlayerRegex = new Regex(@"\[Power\] GameState\.DebugPrintEntityChoices\(\) - id=(.) Player=(.+) TaskList=(.) ChoiceType=MULLIGAN CountMin=. CountMax=.");
		public static Regex findFirstPlayerRegex = new Regex(@"\[Power\] PowerTaskList\.DebugPrintPower\(\) -     TAG_CHANGE Entity=(.+) tag=(FIRST_PLAYER) value=1");
		public static Regex findDamage = new Regex(@"\[Power\] GameState\.DebugPrintPower\(\) -     TAG_CHANGE Entity=\[name=(.+) id=.+ zone=.+ zonePos=.+ cardId=.+ player=(.)\] tag=PREDAMAGE value=(.+)");
		public static Regex findCurrentTurn = new Regex(@"\[Power\] PowerTaskList\.DebugPrintPower\(\) -     TAG_CHANGE Entity=(.+) tag=(CURRENT_PLAYER) value=(.)");

		public static Target firstPlayer;
		public static Target currentTurn;

		/// <summary>
		/// Unused. Was meant to stop any vibrations
		/// </summary>
		public static void Panic() {
			panicMode = true;
			reading = false;
		}

		/// <summary>
		/// Reads the log
		/// </summary>
		public async static void Read() {
			player.playerID = 1;
			opponent.playerID = 2;
			reading = true;

			System.Diagnostics.Debug.WriteLine("Starting to read");
			if (file == null) {
				var picker = new Windows.Storage.Pickers.FileOpenPicker();
				picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
				picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.ComputerFolder;
				picker.FileTypeFilter.Add(".txt");
				System.Diagnostics.Debug.WriteLine("Picking file");
				ClosedFile = await picker.PickSingleFileAsync();
				System.Diagnostics.Debug.WriteLine("file picked");
			}
			while (Reading) {
				ReadNew();
				await Task.Delay(TimeSpan.FromMilliseconds(200));
			}

		}

		/// <summary>
		/// Read new lines since last update
		/// </summary>
		private async static void ReadNew() {
			/*(await ClosedFile.CopyAsync(ApplicationData.Current.LocalFolder, "logCopy.txt", NameCollisionOption.ReplaceExisting))*/ //A nasty way to read the file
			file = (await ClosedFile.OpenAsync(FileAccessMode.ReadWrite, StorageOpenOptions.AllowReadersAndWriters));
			file.Seek(lastIndex);
			lastIndex = file.Size;
			using (var dataReader = new Windows.Storage.Streams.DataReader(file)) {
				try {
					uint numBytesLoaded = await dataReader.LoadAsync((uint)lastIndex);
					string text = dataReader.ReadString(numBytesLoaded);
					ProcessVibrations(SearchThroughLines(text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)));
				} catch (System.ObjectDisposedException e) {
					Debug(e.StackTrace);
				}
			}
		}

		/// <summary>
		/// Stop reading the log
		/// </summary>
		public static void Stop() {
			reading = false;
		}

		/// <summary>
		/// Looks through the lines and decides what to do with them.
		/// </summary>
		/// <param name="lines"></param>
		/// <returns></returns>
		public static List<VibrationData> SearchThroughLines(string[] lines) {
			List<VibrationData> vibrations = new List<VibrationData>();
			Match match = null;
			for (int i = 0; i < lines.Length; i++) {
				// Finds and assigns the players
				match = findPlayerRegex.Match(lines[i]);
				if (match.Success) {
					if (match.Groups[1].Value.Equals("1")) {
						player.playerName = match.Groups[2].Value;
						Debug("Found first player: ID=1 NAME=" + player.playerName);
					} else if (match.Groups[1].Value.Equals("2")) {
						opponent.playerName = match.Groups[2].Value;
						Debug("Found opponent player: ID=2 NAME=" + opponent.playerName);
					}
					continue;
				}
				// Finds which player is first
				match = findFirstPlayerRegex.Match(lines[i]);
				if (match.Success) {
					Debug("FirstPlayer: " + match.Groups[1].Value);
					if (match.Groups[1].Value.Equals(player.playerName)) {
						firstPlayer = Target.FRIEND;
					} else {
						firstPlayer = Target.ENEMY;
					}
					continue;
				}
				// Finds who's turn it is
				match = findCurrentTurn.Match(lines[i]);
				if (match.Success) {
					if (match.Groups[3].Value.Equals("1")) {
						if (match.Groups[1].Value.Equals(player.playerName)) {
							currentTurn = Target.FRIEND;
						} else {
							currentTurn = Target.ENEMY;
						}
						Debug("Starting turn of: " + currentTurn);
					}
					continue;
				}
				// Finds damage
				match = findDamage.Match(lines[i]);
				if (match.Success) {
					if (!match.Groups[3].Value.Equals("0")) {
						int damage = int.Parse(match.Groups[3].Value);
						Debug(currentTurn + " and "+ vibrateOnFriendly + " and "+vibrateOnEnemy + " AND "+ match.Groups[2].Value + " and "+friendlyTaker);
						if (currentTurn == Target.FRIEND && vibrateOnFriendly) {
							if (int.Parse(match.Groups[2].Value).Equals((int)friendlyTaker)) {
								vibrations.Add(new VibrationData(Target.FRIEND, DataType.DAMAGE, damage));
								Debug("Adding " + currentTurn + ";Damage;Friendly;" + damage);
							}
						} else if(vibrateOnEnemy) {
							if (int.Parse(match.Groups[2].Value).Equals((int)enemyTaker)) {
								vibrations.Add(new VibrationData(Target.ENEMY, DataType.DAMAGE, damage));
								Debug("Adding " + currentTurn + ";Damage;Enemy;" + damage);
							}
						}
					}
					continue;
				}
			}
			return vibrations;
		}

		/// <summary>
		/// Processes the vibrations and decides the vibration length and strenght.
		/// </summary>
		/// <param name="vibrations"></param>
		public static void ProcessVibrations(List<VibrationData> vibrations) {
			if (vibrations.Count <= 0) {
				return;
			}
			if (vibrations.Count(x => x.target == Target.ENEMY) > 8) {
				return;
			}
			if (vibrations.Count(x => x.target == Target.FRIEND) > 8) {
				return;
			}
			int sum = 0;
			for (int i = 0; i < vibrations.Count; i++) {
				Debug(vibrations[i]);
				sum += vibrations[i].strenght.Clamp(0, 10);
			}
			sum /= vibrations.Count;
			sum = sum.Clamp(0, 10);
			sum *= 2;
			Vibrate(sum, sum*100);
		}

		/// <summary>
		/// Actually vibrates the Hush
		/// </summary>
		/// <param name="strenght"></param>
		/// <param name="lengthMS"></param>
		public async static void Vibrate(int strenght, int lengthMS) {
			strenght = strenght.Clamp(0, 20);
			await Task.Delay(delay);
			if (stopVibeTask!=null) {
				stopVibeTask.stop = true;
				stopVibeTask = null;
			}
			var buffer = new DataWriter();
			buffer.WriteString("Vibrate:"+strenght+";");
			buffer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
			await VibeChar.WriteValueAsync(buffer.DetachBuffer());
			Debug("Vibrating at " + strenght);
			stopVibeTask = new CancelTask();
			StopVibeAfterTime(lengthMS, stopVibeTask);
		}

		/// <summary>
		/// Stops vibration after lengthMS
		/// </summary>
		/// <param name="lengthMS"></param>
		/// <param name="ct"></param>
		public async static void StopVibeAfterTime(int lengthMS, CancelTask ct) {
			await Task.Delay(lengthMS);
			if (ct.stop) {
				return;
			}
			var buffer = new DataWriter();
			buffer.WriteString("Vibrate:0;");
			buffer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
			await VibeChar.WriteValueAsync(buffer.DetachBuffer());
			Debug("Stopping vibrations");
		}

		/// <summary>
		/// Starts a BluetoothLEAdvertisementWatcher
		/// </summary>
		public static void FindHush() {
			Debug("Finding");
			BluetoothLEAdvertisementWatcher watcher = new BluetoothLEAdvertisementWatcher();
			System.Diagnostics.Debug.WriteLine(watcher.Status);
			watcher.Received += Watcher_Received;
			watcher.ScanningMode = BluetoothLEScanningMode.Active;
			watcher.Start();
		}

		/// <summary>
		/// Initializes Hush
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="args"></param>
		private async static void Watcher_Received(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args) {
			//Debug("Found Device");
			if (args.Advertisement.LocalName.Contains("LVS")) {
				sender.Received -= Watcher_Received;
				Debug("[" + args.Timestamp + "]Found LVS - " + args.Advertisement.Flags + " --- " + args.AdvertisementType + " --- " + args.RawSignalStrengthInDBm);
				//Debug(args.BluetoothAddress);
				sender.Stop();
				//Debug("Finding by address");

				Hush = await BluetoothLEDevice.FromBluetoothAddressAsync(args.BluetoothAddress);
				Debug(Hush == null);
				//Debug("Finding services");
			}

		}

		/// <summary>
		/// Just a shortcut
		/// </summary>
		/// <param name="str"></param>
		public static void Debug(object str) {
			System.Diagnostics.Debug.WriteLine(str);
		}

		/// <summary>
		/// Finds the correct GATT services
		/// </summary>
		public async static void FindServices() {
			var services = (await Hush.GetGattServicesAsync()).Services;
			GattDeviceService service = services[2];
			//6e400001-b5a3-f393-e0a9-e50e24dcca9e
			var chars = (await service.GetCharacteristicsAsync()).Characteristics;
			BatteryChar = chars[0];
			VibeChar = chars[1];
		}
	}
}
