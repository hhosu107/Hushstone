﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HearthHush {
	static class Extensions {

		/// <summary>
		/// Adds a Clamp method to every IComparable
		/// </summary>
		/// <typeparam name="T">Type</typeparam>
		/// <param name="val">Value</param>
		/// <param name="min">Minimal value</param>
		/// <param name="max">Maximal value</param>
		/// <returns></returns>
		public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T> {
			if (val.CompareTo(min) < 0) return min;
			else if (val.CompareTo(max) > 0) return max;
			else return val;
		}
	}
}
